;(function () {
    angular
        .module("givaldo-app")
        .controller("CrudController", ["$scope", function ($scope) {


            $scope.users = [

                {
                    nome: "Givaldo Marques dos Santos",
                    id: "113132",
                    fone: "(79) 98158-3468"
                },

                {
                    nome: "Pedro Henrique Leite",
                    id: "213442",
                    fone: "(79) 11111-2222"
                },

                {
                    nome: "Italo Carvalho",
                    id: "395204",
                    fone: "(79) 33333-4444"
                },

                {
                    nome: "Paulo de Brito",
                    id: "490344",
                    fone: "(79) 55555-6666"
                }

            ];

            $scope.user = {};
            $scope.showMessage = false;


            $scope.removeUser = function(id){
                this.users.splice(id,  1);
            };

            $scope.createUser = function () {

                for(var i = 0; i < this.users.length; i++){
                    if(this.user.id === this.users[i].id){
                        this.showMessage = true;
                        return;
                    }
                }
                this.users.splice(this.users.length, 0, this.user);
                this.user = {};
                this.showMessage = false;

            };

            $scope.editUser = function (id) {
                this.user.nome = this.users[id].nome;
                this.user.id = this.users[id].id;
                this.user.fone = this.users[id].fone;
                this.removeUser(id);
            };

        }]);
})();
